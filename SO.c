#include <unistd.h> //getpid(), fork(),execlp()
#include <stdlib.h> //atoi, exit
#include <stdio.h> //si tengo print
#include <sys/types.h> //si tengo sigaction
#include <sys/wait.h> //si tengo waitpid
#include <string.h> //si uso string
#include <signal.h> //si uso signals
#include <fcntl.h> //open i cosas
#include <sys/stat.h>


void usage(){
    char buffer[256];
    sprintf(buffer,"Usage:listaParametros arg1 [arg2..argn]\n Este programa escribe por su salida la lista de argumentos que recibe\n");
    write(1,buffer,strlen(buffer));
    exit(1);
}

void error_y_exit(char *msg,int exit_status){
    //char buffer[256];
    //sprintf(buffer,msg);
    //write(1,buffer,strlen(buffer));
    perror(msg);
    exit(exit_status);
}

void trataExitCode(int pid,int exit_code){
    int statcode,signcode;
    char buffer[128];
    if (WIFEXITED(exit_code)) {
        statcode = WEXITSTATUS(exit_code);
        sprintf(buffer,"El proceso %d termina con exit code %d\n", pid, statcode);
        write(1,buffer,strlen(buffer));
    }
    else {
        signcode = WTERMSIG(exit_code);
        sprintf(buffer,"El proceso %d termina por el signal %d\n", pid, signcode);
        write(1,buffer,strlen(buffer));
    }
}

void imprimir(char *msg){
    char buffer[256];
    sprintf(buffer, msg);
    write(1,buffer,strlen(buffer));
}

void funcio_handler(int s){
    if (s == SIGALRM){
        imprimir("Signal rebut SIGALRM");
    }
}

void mainSignals(){
    sigset_t mask;
    struct sigaction trat;
    
    //Bloqueamos para que no moleste
    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM); //añade signal
    sigprocmask(SIG_BLOCK,&mask, NULL);
    
    //Capturem signal
    sigemptyset(&trat.sa_mask);
    trat.sa_handler = funcio_handler;
    trat.sa_flags = 0;
    sigaction(SIGALRM, &trat, NULL);
    
    //Sigsuspend
    sigfillset(&mask);
    sigdelset(&mask, SIGALRM); //deletea signal
    sigsuspend(&mask);
}

void mainFork(){
    int pid, exit_code;
    int n = 5;
    for(int i = 0; i < n; ++i){
        if ((pid = fork())<0) error_y_exit("Error en fork",1);
        else if (pid == 0){
            //codi fill
            exit(0); //si no volem crear més procesos
        }
        else{
            //si volem sequencial
            if ((pid = waitpid(-1,&exit_code,0)) > 0) trataExitCode(pid, exit_code);
        }
    }
    //si volem concurrent
    while((pid = waitpid(-1,&exit_code,0)) > 0) trataExitCode(pid, exit_code);
    exit(0);
}

void main (int argc, char *argv[]) 
{
	//argc >= 1 siempre
    if (argc != 2) usage(); //Si no rep només 1 parametre
    if (atoi(argv[1]) == 0) error_y_exit("Has pasado un 0\n", 1);
    int pid;
    if ((pid = fork()) < 0) error_y_exit("Error en fork",1);
    if (pid == 0) exit(5); //o exit(5);
    int pid2 = fork();
    if (pid2 == 0){
        char buf[10];
        int a = atoi(argv[1]) - 1;
        sprintf(buf,"%d",a);
        execlp("./funciones_so","funciones_so",buf,(char*) NULL);
        error_y_exit("EXCLP falla",2);
    }
    int exit_code;
    imprimir("Soy el padre ");
    waitpid(pid,&exit_code,0);
    trataExitCode(pid,exit_code);
    while(1);
}

void esquema1(){
    //WAITPID
        int exit_code;
        //waitpid(pid,&exit_code,0);
        //pid = -1 -> cualquier hijo
        //pid = real_pid -> hijo con real_pid
        //&exit_code -> como ha acabado (si no interesa -> NULL)
        //WNOHANG -> No bloquea
        //RETURN:
            //pid si hijo pid acaba
            //error: -1
            //0 si WNOHANG y no cambia de estado el hijo
        
    //EXECLP
        //execlp("./hola","hola",parametro1,parametro2,(char*) NULL);
	//EXECVP
		//execvp(programa, argv);
		
    //SIGPROCMASK
        sigset_t mask;
        
        sigemptyset(&mask);
        sigaddset(&mask, SIGALRM); //añade signal
        sigprocmask(SIG_BLOCK,&mask, NULL);
        //SIG_BLOCK: Bloquea los que esten a 1
        //SIG_UNBLOCK: Desbloquea los que esten a 1
        //SIG_SETMASK: Pone la mascara
    
// LOS BLOQUEADOS SE QUEDAN PENDIENTES
    
    //SIGISMEMBER
        sigismember(&mask, SIGKILL); //devuelve si esta en la mascara
    
    //SIGACTION
        struct sigaction trat;
        
        sigemptyset(&trat.sa_mask);
        trat.sa_handler = funcio_handler;
        trat.sa_handler = SIG_DFL;
        trat.sa_handler = SIG_IGN;
        trat.sa_flags = 0;
        // = SA_RESETHAND (vuelve al tratamiento por defecto)
        // = SA_RESTART (se reinicia la llamada que lo había bloqueado)
        sigaction(SIGCHLD, &trat, NULL);
    
    //SIGSUSPEND
        sigfillset(&mask);
        sigdelset(&mask, SIGALRM); //deletea signal
        sigsuspend(&mask);
        
	//ERRORES
		//kill(pid, signal) -1 -> error (< 0)
		//sigaction (signal, &trat, NULL) -1 -> error (< 0)
		//fork() -1 -> error (< 0)
		//execlp() always return -1 (if return, there's an error)
        //sigsuspend always return -1 (not an error) (!= -1)
        //sigprocmask -1 -> error (< 0)
		
    //COMANDOS
        //kill -sigkill PID
}

if ((fd = open(documento, O_RDONLY)) < 0) error_y_exit("Error al abrir",1);

void esquema2(){
    printf("Escriure coses, i fins i tot numeros com %d.\n",10);
	char* numeros = argv[1]; //Pillar nombre documento (para hacer opens i cosas)
	//OPEN
	//int fd = open("nom_fixer",Permisos+flags, if (flag=O_CREAT))
	//Permisos = O_RDONLY, O_WRONLY, O_RDWR
	//Flags = O_CREAT, O_TRUNC
	//Extra con O_CREAT (do man 2 open or foto)
	//Exemple if ((fd2=open("suma.txt",O_RDWR|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR)) < 0) error_y_exit("Error en open",1);
	
		/* R -> Read */
		/* W -> Write */
		/* X -> Execute */

		/* Owner -> Who creates the file */
		/* Group -> Users who are in the group of the owner */
		/* Other -> Someone else */
	
		#define S_IRWXU 0000700    /* RWX mask for owner */
		#define S_IRUSR 0000400    /* R for owner */
		#define S_IWUSR 0000200    /* W for owner */
		#define S_IXUSR 0000100    /* X for owner */

		#define S_IRWXG 0000070    /* RWX mask for group */
		#define S_IRGRP 0000040    /* R for group */
		#define S_IWGRP 0000020    /* W for group */
		#define S_IXGRP 0000010    /* X for group */

		#define S_IRWXO 0000007    /* RWX mask for other */
		#define S_IROTH 0000004    /* R for other */
		#define S_IWOTH 0000002    /* W for other */
		#define S_IXOTH 0000001    /* X for other */
		
	//READ
	//int leido = read(fd,&c,sizeof(c)), read(fd,buf,sizeof(buf));
	
	//WRITE
	//int escrito = write(fd,&c,sizeof(c)), write(fd,buf,sizeof(buf));
	
	//LSEEK
	//lseek(fd,offset,choose);
	//choose = SEEK_END, SEEK_CUR, SEEK_SET
	
	//DUP
	//dup2(pd[1],1) == Close de 1, duplica pd[1] i lo mete en 1. (cerrar duplicación al final)
	
	//PIPES SIN NOMBRE
	//int pd[2]; (come por pd[1]) pd[1]----->pd[0] (caga por pd[0]) 
	//pipe(pd); Crea la pipe
	//Cerrar o bloqueoss
	
	//PIPES CON NOMBRE
	//mknod("NOMBRE_PIPE",S_IFIFO| permisos, 0)
	//Para controlar si existe que no salte error -> 
	//if (mknod("NOMBRE_PIPE",S_IFIFO| permisos(en octal, los del open), 0) < 0){
	//	if(errno != EEXIST) error_y_exit("error pipe con nombre",1);
	//}
	//Al hacer un open("NOMBRE_PIPE",O_WRONLY) Le damos los permisos que usaremos
	//Cerrar tmb
	
	//MALLOC
	//char* c = malloc(numero_de_bytes)
	//int* num = malloc(numero de bytes)
	//El heap augmenta mucho mas de lo pedido
	//Podemos acceder c[i] y num[i]
	//Al hacer write o read(0,c,numero_a_leer)
	//free(c), free(num)
	
	//SBRK
	//char* c = sbrk(numero_de_bytes)
	//int* num = sbrk(numero de bytes)
	//Podemos acceder c[i] y num[i]
	//Al hacer write o read(0,c,numero_a_leer)
	//Al final sbrk(-numero_de_bytes)
}

void segundo_examen_terminal(){
	//REDIRIGIR A TERMINAL 
	//./es > /dev/pts/2 (ruta al terminal vista en ps -a o ps) 
	//o con pipes con nombre:
	//mknod mipipe p
	//T1: in.txt > mipipe   T2: cat < /direccion_pipe/mipipe
	
	//PONER WHILE(waitpid(-1,NULL,0) > 0){} al final si hacemos hijos
	
	//PIPES
	//Sin nombre = |
	//Con nombre = mknod mipipe p
	
	//OTROS
    // strace = lista de llamadas a sistema ejecutadas por un proceso (cantidad con -c)
		//uso: strace –o salida_v2 –e read ./ejecutable (-e exactamente un tipo de llamada a sistema)
    // mknod = crea fichero especial  (mknod mipipe p)
    // ps = muestra informacion sobre un proceso en ejecucion
    // En /proc/devices ver los majors
    // | pipe sin nombre por consola
    // ln crea links a ficheros (-s soft) (sin -s hard link)
    // stat muestra informacion de control de un fichero (numero de links, inodo etc)
    // namei procesa ruta hasta el final
    // readlink lee el contenido de un link simbolico
    // df devuelve informacion sobre el sistema de ficheros (df -i == inodos libres)
	
	//MEMORIA
	//cd/proc/id(del proceso) - cat map
	//nm tabla de simbolos del programa (variables globales etc)
	//objdump muestra informacion sobre el fichero objeto
}

